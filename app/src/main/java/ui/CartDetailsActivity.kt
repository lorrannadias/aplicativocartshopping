package ui

import android.app.Activity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import br.com.shoppingcart.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.adapter_cart_details_main.*
import model.CartResponse
import java.text.DecimalFormat






class CartDetailsActivity : AppCompatActivity() {
    private var current = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        //Toolbar//
        setContentView(R.layout.adapter_cart_details_main)
        setSupportActionBar(toolbar);
        val toolbar = findViewById(R.id.toolbardetail) as Toolbar
        setSupportActionBar(toolbar)
        if (getSupportActionBar() != null) {
            getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
            getSupportActionBar()?.setHomeButtonEnabled(true)
            getSupportActionBar()?.setDisplayShowHomeEnabled(true)
        }
        toolbar.setNavigationIcon(R.mipmap.ic_close_black_24dp) //your icon
        toolbar.setNavigationOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View) {
                finish();
            }
        })

        val cartResponse : CartResponse

        cartResponse = intent.getSerializableExtra("myKey") as CartResponse
        if (cartResponse != null) {
            tv_cartitem_name.text = cartResponse.name
            tv_cartitem_desc.text = cartResponse.description
            tv_cartitem_price.text = trataValue(cartResponse.price)
            tv_cartitem_subtitle.text = getStatus(cartResponse.stock!!)
            Picasso.get().load(cartResponse.image_url).into(mv_item_select)
        }





    }
    fun getStatus(qtde : Int):String{
        if(qtde >= 3){
            return "Em estoque"
        }else if(qtde> 0 && qtde <= 2){
            return "Em estoque apenas $qtde"
        }
        return "sem estoque"
    }


    //cardResponse.price

    fun trataValue(value: Int?) : String{
        var str = String()
        val twoPlaces = DecimalFormat("#,##,##")

        try {
            if(value != null){
                str = twoPlaces.format(value)
            }
        }catch (e : Exception){
            e.message.toString()
        }
        return "$ ${str.toString()}"
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_actionbardetail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_search -> true
            else -> super.onOptionsItemSelected(item)
        }
    }


}




