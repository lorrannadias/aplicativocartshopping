package ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.shoppingcart.R
import br.com.shoppingcart.adapter.CartAdapter
import kotlinx.android.synthetic.main.activity_main.*
import model.CartResponse
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.jetbrains.anko.yesButton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import service.APIService
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {

    var produtos = ArrayList<CartResponse>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
        {
            getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
            getSupportActionBar()?.setHomeButtonEnabled(true)
            getSupportActionBar()?.setDisplayShowHomeEnabled(true)
        }
        toolbar.setNavigationIcon(R.mipmap.ic_dehaze_black_24dp) //your icon
        toolbar.setNavigationOnClickListener(object: View.OnClickListener {
            override fun onClick(v:View) {
                //Do whatever you want to do here
            }
        })

        initCharacter()
        searchByName()

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_actionbar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_search -> true
            else -> super.onOptionsItemSelected(item)
        }
    }



    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://raw.githubusercontent.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    private fun searchByName() {
        doAsync {
            val call = getRetrofit().create(APIService::class.java).getCharacterByName().execute()
            uiThread {
                if (call.errorBody() == null) {
                    produtos = call.body() as ArrayList<CartResponse>

                    var adapter = rv_cart_list.adapter as CartAdapter
                    adapter.items = produtos
                    adapter.notifyDataSetChanged()
                    tv_total_soma.text = trataValue(getTotal(produtos))
                    tv_subtotal_soma.text = trataValue(getSubtotal(produtos))
                    tv_shipping_soma.text = trataValue(getShipping(produtos))
                    tv_tax_soma.text = trataValue(getTax(produtos))
                    tv_quantity_item.text = getSomaItens(produtos)

                } else {
                    showErrorDialog()
                }
            }
        }
    }

    private fun initCharacter() {
        rv_cart_list.layoutManager = LinearLayoutManager(this) as RecyclerView.LayoutManager?
        rv_cart_list.adapter = CartAdapter(ArrayList(), this)
    }

    private fun showErrorDialog() {
        alert("ocorreu um erro, tente de novo.") {
            yesButton { }
        }.show()
    }

    private fun hideKeyboard() {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
    }

    fun getSomaItens(listProdutos: ArrayList<CartResponse>):String{
       if(listProdutos != null && listProdutos.size > 0){

//           return listProdutos.size.toString()

           return "${listProdutos.size} itens no carrinho"

       }
        return "adicione itens ao carrinho"
    }




    fun getTotal(listProdutos: ArrayList<CartResponse>): Int? {
        var soma = 0
        for (carResponse in listProdutos) {
            soma = soma + carResponse.tax!!
            soma = soma + carResponse.price!!
            soma = soma + carResponse.shipping!!
        }
        return soma
    }

    fun getSubtotal(listProdutos: ArrayList<CartResponse>): Int? {
        var soma = 0
        for (carResponse in listProdutos) {
            soma = soma + carResponse.price!!
        }
        return soma
    }

    fun getShipping(listProdutos: ArrayList<CartResponse>): Int? {
        var soma = 0
        for (carResponse in listProdutos) {
            soma = soma + carResponse.shipping!!
        }
        return soma
    }

    fun getTax(listProdutos: ArrayList<CartResponse>): Int? {
        var soma = 0
        for (carResponse in listProdutos) {
            soma = soma + carResponse.tax!!
        }
        return soma
    }

    fun trataValue(value: Int?) : String{
        var str = String()
        val twoPlaces = DecimalFormat("#,##,##")

        try {
            if(value != null){
                str = twoPlaces.format(value)
            }
        }catch (e : Exception){
            e.message.toString()
        }
        return "${str.toString()}"
    }

}