package br.com.shoppingcart.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.shoppingcart.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.adapter_cart_main.view.*
import model.CartResponse
import org.jetbrains.anko.sdk27.coroutines.onClick
import ui.CartDetailsActivity
import java.text.DecimalFormat


class CartAdapter(var items: List<CartResponse>, var context: Context) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items.get(position))
        holder.itemView.onClick {

            try{
                var cardResponse =  items.get(position)

                context.startActivity(Intent(context, CartDetailsActivity::class.java).putExtra("myKey", cardResponse))
            }catch (e: Exception){

            }
        }


    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.adapter_cart_main, parent, false))


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvCardType = view.tv_produto_list
        val mvProduto = view.mv_produto_list
        val tvValor = view.tv_valor_list
        val tvStock = view.tv_stock_list


        fun bind(cartReponse: CartResponse) {
            tvCardType?.text = cartReponse.name
            tvStock?.text = getStatus(cartReponse.stock!!)
            tvValor?.text = trataValue(cartReponse.price)
            Picasso.get().load(cartReponse.image_url).into(mvProduto)

        }

        fun getStatus(qtde : Int):String{
            if(qtde >= 3){
                return "Em estoque"
            }else if(qtde> 0 && qtde <= 2){
                return "Em estoque apenas $qtde"
            }
            return "sem estoque"
        }

        fun trataValue(value: Int?) : String{
            var str = String()
            val twoPlaces = DecimalFormat("#,##,##")

            try {
                if(value != null){
                    str = twoPlaces.format(value)
                }
            }catch (e : Exception){
                e.message.toString()
            }
            return "${str.toString()}"
        }

    }

}

