package service

import model.CartResponse
import retrofit2.Call
import retrofit2.http.GET

interface APIService {
    @GET("myfreecomm/desafio-mobile-android/master/api/data.json")
    fun getCharacterByName(): Call<List<CartResponse>>

}