package model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CartResponse : Serializable {

    @SerializedName("name")
    var name : String? = null

    @SerializedName("quantity")
    var quantity : Int? = null

    @SerializedName("stock")
    var stock : Int? = null

    @SerializedName("image_url")
    var image_url : String? = null

    @SerializedName("price")
    var price : Int? = null

    @SerializedName("subtotal")
    var subtotal : Int? = null

    @SerializedName("tax")
    var tax : Int? = null

    @SerializedName("shipping")
    var shipping : Int? = null

    @SerializedName("description")
    var description : String? = null


}